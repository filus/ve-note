import { BrowserWindow, dialog, Menu } from 'electron'
import { Handler } from './constant.js'
import MenuItemConstructorOptions = Electron.MenuItemConstructorOptions
import MessageBoxOptions = Electron.MessageBoxOptions
// 创建 menu
export default function createMenu() {
  const menuStructure: MenuItemConstructorOptions[] = [
    {
      label: '菜单',
      submenu: [
        {
          label: '初始化',
          click() {
            const b = BrowserWindow.getAllWindows()
            b[0].webContents.send(Handler.goRouter, 'welcome')
            console.log('重新开始！')
          },
        },
        {
          label: '配置',
          click() {
            const message: MessageBoxOptions = {
              message: '点击了配置，但是现在没什么用',
            }
            dialog.showMessageBox(message).then(() => {})
            console.log('点击了配置，但是现在没什么用')
          },
        },
        {
          label: '刷新', // 刷新页面
          click() {},
          role: 'reload',
        },
        {
          label: '切换调试窗口',
          role: 'toggleDevTools',
        },
      ],
    },
    {
      label: '关于',
      submenu: [
        {
          label: '最小化',
          role: 'minimize',
        },
        {
          label: '关于',
          role: 'about',
        },
        { type: 'separator' },
        {
          label: '退出',
          role: 'quit',
        },
      ],
    },
  ]
  const menu = Menu.buildFromTemplate(menuStructure)
  Menu.setApplicationMenu(menu)
}
