export const Handler = {
  showOpenDialog: 'dialog.showOpenDialog',
  selectFile: 'dialog.selectFile',
  openPath: 'dialog.openPath',
  readFile: 'dialog.readFile',

  getFileList: 'fs.getFileList',

  saveAppSetting: 'app.saveAppSetting',
  clearAppSetting: 'app.clearAppSetting',
  getAppSetting: 'app.getAppSetting',

  goRouter: 'general.goRouter',
  execSoftware: 'general.execSoftware',
}
