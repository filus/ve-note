import { ipcMain } from 'electron'
import { Handler } from '../constant/constant.js'

// 基础控制器
export default function generalHandlerSetup() {
  console.debug('load generalHandlerSetup')

  ipcMain.handle(Handler.execSoftware, () => {})
}
