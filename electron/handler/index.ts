import { appSettingHandler } from './AppSettingHandler.js'
import { fileHandlerSetup } from './FileHandler.js'
import generalHandlerSetup from './GeneralHandler.js'

export function handlerSetup() {
  console.info('handlerSetup')
  appSettingHandler()
  fileHandlerSetup()
  generalHandlerSetup()
}
