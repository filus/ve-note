import type { AppSetting } from '../entity/appSetting.js'
import { ipcMain } from 'electron'
import _ from 'lodash'
import { Handler } from '../constant/constant.js'
import appSettingDB from '../database/project.js'

// 系统设置控制器
export function appSettingHandler() {
  console.debug('load appSettingHandler')

  /**
   * 保存设置
   */
  ipcMain.handle(Handler.saveAppSetting, (event, args: AppSetting) => {
    if (_.isEmpty(args)) {
      return null
    }
    return appSettingDB.addAppSetting(args)
  })

  /**
   * 清除设置
   */
  ipcMain.handle(Handler.clearAppSetting, () => {
    return appSettingDB.deleteAppSetting(1)
  })

  /**
   * 获取设置
   */
  ipcMain.handle(Handler.getAppSetting, (): AppSetting => {
    return appSettingDB.getAppSetting('')
  })
}
