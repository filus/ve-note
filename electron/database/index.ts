import appSettingDB from './project.js'

export interface DBConfig {
  initTable: () => void
  tableName: string
  tableVersion: number
  // initData?: () => void;
}

export function initDb() {
  console.info('initDb')
  appSettingDB.initTable()
}
