import Store from 'electron-store'

const store = new Store()

export function getDB() {
  return store
}
