export class AppSetting {
  /**
   * 编号
   */
  id: number
  /**
   * 首次启动
   */
  first: boolean
  /**
   * 昵称
   */
  nikeName: string
  /**
   * 工作空间路径
   */
  path: string
  /**
   * 工作空间名称
   */
  projectName: string
  /**
   * 文件排序
   */
  sortType: Sort
}

export interface PageObj<T> {
  total: number
  data: T[]
}

export interface PageQuery {
  start: number
  size: number
}

export interface Sort {
  normal: '默认排序'
  nameAscending: '名称递增'
  nameDescending: '名称递减'
  typeAscending: '类型递增'
  typeDescending: '类型递减'
  sizeAscending: '大小递增'
  sizeDescending: '递减'
  dateModifiedAscending: '修改日期递增'
  dateModifiedDescending: '修改日期递减'
}
