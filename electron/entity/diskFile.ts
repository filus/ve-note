export class DiskFile {
  /**
   * inode 编号0
   */
  ino: bigint
  /**
   * 文件名称
   */
  name: string
  /**
   * 文件路径
   */
  parentPath: string
  /**
   * 是否是文件夹
   */
  folder: boolean
  /**
   * 文件类型
   */
  type: string
  /**
   * 文件名后缀
   */
  suffix: string
  /**
   * 文件图标
   */
  icon: string
  /**
   * 文件图标
   */
  iconPath: string
  /**
   * 文件大小
   */
  size: bigint
  /**
   * 文件的最后访问时间
   */
  atime: string
  /**
   * 文件的最后修改时间（内容修改）
   */
  mtime: string
  /**
   * 文件的最后状态改变时间（如权限或所有权变更）
   */
  ctime: string
  /**
   * 文件的创建时间
   */
  birthtime: string
}
