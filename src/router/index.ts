import { createRouter, createWebHashHistory } from 'vue-router'

const constantRouter = [
  {
    path: '/',
    component: () => import('../view/IndexLayout.vue'),
    children: [],
    meta: { title: '首页' },
  },
  {
    path: '/welcome',
    component: () => import('../view/welcome/WelcomePage.vue'),
    children: [],
    meta: { title: '初始化' },
  },
]

const router = createRouter({
  routes: constantRouter,
  history: createWebHashHistory(),
})

export default router
