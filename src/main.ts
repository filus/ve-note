import { customIpcRenderer } from '@/renderer/CustomRenderer.js'
import ArcoVue from '@arco-design/web-vue'

import { createPinia } from 'pinia'
import { createApp } from 'vue'
import App from './App.vue'
import router from './router/index.js'
import './assets/main.css'
import 'virtual:uno.css'
import '@arco-design/web-vue/dist/arco.css'

const app = createApp(App)
const pinia = createPinia()

customIpcRenderer()
app.use(pinia)
app.use(ArcoVue as any)
app
  .use(router)
  .mount('#app')
  .$nextTick(() => {})
  .then(() => {})
