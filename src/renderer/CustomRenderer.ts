import router from '@/router/index.js'
import { Handler } from '@@/constant/constant.js'
import IpcRendererEvent = Electron.IpcRendererEvent

export function customIpcRenderer() {
  window.ipcRenderer.on(
    Handler.goRouter,
    (event: IpcRendererEvent, ...args: any[]) => {
      console.log('goRouter', event, ...args)
      void router.push(args[0])
    },
  )
}
