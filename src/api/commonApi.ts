import { Handler } from '@@/constant/constant.js'
import { AppSetting } from '@@/entity/appSetting.js'
import { DiskFile } from '@@/entity/diskFile.js'

export default {
  /**
   * ---file start---
   */
  /**
   * 打开文件窗口
   */
  showOpenDialog: () => {
    return window.ipcRenderer.invoke(Handler.showOpenDialog) as Promise<string>
  },
  /**
   * 选择文件
   */
  selectFile: () => {
    return window.ipcRenderer.invoke(Handler.selectFile) as Promise<string>
  },
  /**
   * 读取文件内容
   */
  readFile: (filePath: string) => {
    return window.ipcRenderer.invoke(Handler.readFile, filePath) as Promise<string>
  },
  /**
   * 打开指定路径的文件
   */
  openPath: (filePath: string) => {
    return window.ipcRenderer.invoke(Handler.openPath, filePath) as Promise<string>
  },
  /**
   * 获取文件夹内文件列表
   */
  getFileList: (path: string) => {
    return window.ipcRenderer.invoke(Handler.getFileList, path) as Promise<DiskFile[]>
  },

  /**
   * ---appSet start---
   */
  /**
   * 保存软件设置
   * @param appSetting
   */
  saveAppSetting: (appSetting: AppSetting) => {
    return window.ipcRenderer.invoke(Handler.saveAppSetting, appSetting) as Promise<boolean>
  },
  /**
   * 获取软件设置
   */
  getAppSetting: () => {
    return window.ipcRenderer.invoke(Handler.getAppSetting) as Promise<AppSetting>
  },
}
