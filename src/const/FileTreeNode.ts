import type { TreeNodeData } from '@arco-design/web-vue'

export interface FileTreeNode extends TreeNodeData {
  key: string | number
  parentPath?: string
  iconPath?: string

  suffix?: string
  /**
   * 是否是文件夹
   */
  folder?: boolean
  /**
   * 文件夹是否打开
   */
  open?: boolean
}
