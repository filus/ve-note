import {
  getIconForFile,
  getIconForFolder,
  getIconForOpenFolder,
} from './icon/Index.js'

// 定义常见文件类型枚举
export enum FileType {
  Text = 'Text', // 文本文件
  JSON = 'JSON', // JSON 文件
  Markdown = 'Markdown', // Markdown 文件（.md）
  Script = 'Script', // 脚本文件（如shell脚本、PowerShell脚本等）
  RichText = 'RichText', // 富文本文件（如.rtf）
  Media = 'Media', // 媒体文件
  Music = 'Music', // 音乐文件
  Code = 'Code', // 代码文件
  Archive = 'Archive', // 压缩文件
  DiskImage = 'DiskImage', // 磁盘映像文件（如ISO）
  Image = 'Image', // 图像文件
  VectorImage = 'VectorImage', // 矢量图像文件
  Font = 'Font', // 字体文件
  Database = 'Database', // 数据库文件
  Executable = 'Executable', // 可执行文件
  Package = 'Package', // 软件包文件
  PDF = 'PDF', // PDF 文档文件
  Document = 'Document', // 通用文档文件
  Spreadsheet = 'Spreadsheet', // 电子表格文件
  Presentation = 'Presentation', // 演示文稿文件
  XML = 'XML', // XML/HTML 文件
  CAD = 'CAD', // CAD 绘图文件
  Ebook = 'Ebook', // 电子书文件
  Unknown = 'unknown',
}

const fileExtensionMap: { [key: string]: FileType } = {
  // 文本文件
  'txt': FileType.Text,
  'ini': FileType.Text,
  'log': FileType.Text,
  'conf': FileType.Text,
  'cmd': FileType.Text,

  // json 文件
  'json': FileType.JSON,

  // Markdown 文件（.md）
  'md': FileType.Markdown,

  // 脚本文件（如shell脚本、PowerShell脚本等）
  'sh': FileType.Script,
  'bat': FileType.Script,
  // 富文本文件（如.rtf）
  'rtf': FileType.RichText,

  // 媒体文件
  'mp4': FileType.Media,
  'avi': FileType.Media,
  'mkv': FileType.Media,
  'mov': FileType.Media,
  'flv': FileType.Media,
  'wmv': FileType.Media,
  'mpg': FileType.Media,
  'mpeg': FileType.Media,
  'vob': FileType.Media,
  'webm': FileType.Media,

  // 音乐文件
  'mp3': FileType.Music,
  'wav': FileType.Music,
  'midi': FileType.Music,
  'aac': FileType.Music,
  'flac': FileType.Music,
  'aiff': FileType.Music,
  'ogg': FileType.Media,
  'm4a': FileType.Music,

  // 代码文件
  'config': FileType.Code,
  'properties': FileType.Code,
  'java': FileType.Code,
  'py': FileType.Code,
  'js': FileType.Code,
  'ts': FileType.Code,
  'c': FileType.Code,
  'cpp': FileType.Code,
  'h': FileType.Code,
  'hpp': FileType.Code,
  'cs': FileType.Code,
  'rb': FileType.Code,
  'php': FileType.Code,
  'go': FileType.Code,
  'rust': FileType.Code,
  'scala': FileType.Code,

  // 压缩文件
  'zip': FileType.Archive,
  'tar': FileType.Archive,
  'gz': FileType.Archive,
  'tgz': FileType.Archive,
  'bz2': FileType.Archive,
  'xz': FileType.Archive,
  'rar': FileType.Archive,
  'cab': FileType.Archive,
  '7z': FileType.Archive,

  // 磁盘映像文件（如ISO）
  'iso': FileType.DiskImage,

  // 图像文件
  'jpg': FileType.Image,
  'jpeg': FileType.Image,
  'png': FileType.Image,
  'gif': FileType.Image,
  'bmp': FileType.Image,
  'svg': FileType.VectorImage,
  'tiff': FileType.Image,
  'psd': FileType.Image,
  'webp': FileType.Image,

  // 字体文件
  'ttf': FileType.Font,
  'otf': FileType.Font,
  'fnt': FileType.Font,
  'fon': FileType.Font,
  'pfb': FileType.Font,

  // 数据库文件
  'sql': FileType.Database,
  'db': FileType.Database,
  'mdb': FileType.Database,
  'sqlite': FileType.Database,
  'sqlitedb': FileType.Database,

  // 可执行文件
  'exe': FileType.Executable,

  // 软件包
  'deb': FileType.Package,
  'rpm': FileType.Package,
  'pkg': FileType.Package,
  'ipa': FileType.Package,
  'apk': FileType.Package,
  'dmg': FileType.Package,

  // PDF 文档文件
  'pdf': FileType.PDF,
  'odf': FileType.PDF,

  // 文档文件
  'doc': FileType.Document,
  'docx': FileType.Document,
  'docm': FileType.Document,
  'dot': FileType.Document,
  'dotx': FileType.Document,
  'dotm': FileType.Document,
  'odt': FileType.Document,

  // 电子表格
  'xls': FileType.Spreadsheet,
  'xlsx': FileType.Spreadsheet,
  'xlsm': FileType.Spreadsheet,
  'xlt': FileType.Spreadsheet,
  'xltm': FileType.Spreadsheet,
  'xlam': FileType.Spreadsheet,
  'xlsb': FileType.Spreadsheet,
  'ods': FileType.Spreadsheet,
  'csv': FileType.Spreadsheet,

  // 演示文稿
  'ppt': FileType.Presentation,
  'pptx': FileType.Presentation,
  'pptm': FileType.Presentation,
  'pot': FileType.Presentation,
  'potx': FileType.Presentation,
  'potm': FileType.Presentation,
  'ppam': FileType.Presentation,
  'pps': FileType.Presentation,
  'ppsx': FileType.Presentation,
  'ppsm': FileType.Presentation,
  'sldx': FileType.Presentation,
  'sldm': FileType.Presentation,
  'odp': FileType.Presentation,

  // XML/HTML 文件
  'xml': FileType.XML,
  'html': FileType.XML,
  'htm': FileType.XML,
  'css': FileType.XML,

  // CAD文件
  'dwg': FileType.CAD,
  'dxf': FileType.CAD,

  // 电子书文件
  'epub': FileType.Ebook,
  'mobi': FileType.Ebook,
}

// 函数：根据文件名判断并返回文件类型
export default function getFileType(fileName: string): FileType {
  const extension = fileName.split('.').pop()?.toLowerCase()
  if (!extension) {
    // 如果没有扩展名，返回 Unknown
    return FileType.Unknown
  }
  return fileExtensionMap[extension] || FileType.Unknown
}

const svgToUnocss = function (svgName: string | undefined) {
  if (!svgName) {
    return 'i-vscode-icons:default-file'
  }
  // const withoutExtension = svgName.replace(/\.svg$/, '')
  // 然后，将所有的下划线替换为短横线
  const replaced = svgName.replace(/_/g, '-')
  return `i-vscode-icons:${replaced}`
}

export function iconClass(name: string, folder: boolean, open: boolean) {
  let svg: string | undefined
  if (folder) {
    if (open) {
      svg = getIconForOpenFolder(name)
    }
    else {
      svg = getIconForFolder(name)
    }
  }
  else {
    svg = getIconForFile(name)
  }
  return svgToUnocss(svg)
  // return (svg)
}
